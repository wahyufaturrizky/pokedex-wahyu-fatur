import React, { Component } from 'react';
import { Nav, Container, Input, Form, FormGroup, NavItem, NavLink} from 'reactstrap';
import { Field, reduxForm, InjectedFormProps, Validator } from 'redux-form';
import { connect } from 'react-redux';
import { css } from 'emotion';

import { changePokemon } from "../actions";
import { pokemonNames } from "../assets/ts/name";
import { IPalettes } from "../types";
import { IState } from "../reducers";

import { FaRegCopyright, FaLinkedin, FaGitlab } from "react-icons/fa";

const { LinkedIn, gitlab } = {
  LinkedIn: "https://www.linkedin.com/in/wahyu-fatur-rizky/",
  gitlab: "https://gitlab.com/wahyufaturrizky"
};


interface INavbarProps {
  name: string;
  palettes: IPalettes;
  changePokemon: (index: string) => void;
}

class Searchbar extends Component<
  INavbarProps & InjectedFormProps<{}, INavbarProps>
> {
  onSearchPokemon = (submitProps: any) => {
    const { search } = submitProps;
    const { name, changePokemon } = this.props;

    // If it's same with current pokemon don't do search
    if (name.toLowerCase() === search.toLowerCase()) return;

    // If name is not valid don't do search
    const findIndex = pokemonNames.findIndex(
      availableName => availableName.toLowerCase() === search.toLowerCase()
    );
    if (findIndex === -1) return;

    changePokemon((findIndex + 1).toString());
  };

  renderField = (fieldProps: any) => {
    const { input, meta } = fieldProps;
    const { palettes } = this.props;

    const textColor = `rgb(${palettes.lightMuted})`;
    const borderColor = meta.valid
      ? `rgb(${palettes.lightMuted})`
      : `rgb(${palettes.darkVibrant})`;

    return (
      <Input
        type="text"
        name="pokemon"
        placeholder="Search any Pokémon"
        spellCheck={false}
        className={css`
          text-align: center;
          font-size: 16px;
          color: ${textColor};
          border-color: ${borderColor} !important;
          transition: border-color 1s;
          &::placeholder {
            color: ${textColor};
            transition: color 1s;
          }
        `}
        autoComplete="off"
        invalid={meta.active && !!meta.error}
        list="pokemons"
        {...input}
      />
    );
  };

  render() {
    return (
      <Nav style={{ zIndex: 10 }} className="navbar-transparent w-100 d-flex justify-content-center justify-content-lg-between" expand="lg">
        <NavItem>
        <NavLink className="text-center">
          <FaRegCopyright
            style={{ marginBottom: 3, marginRight: 5, fontSize: 15 }}
          />
          Nintendo, Game Freak, and The Pokémon Company
        </NavLink>
      </NavItem>
        <NavItem>
        <NavLink className="text-white" href={LinkedIn} target="_blank">
          <FaLinkedin style={{ marginRight: 5, fontSize: 18 }} />
          WAHYU FATUR RIZKI
        </NavLink>
      </NavItem>
      <NavItem>
        <NavLink className="text-white mb-4" href={gitlab} target="_blank">
          <FaGitlab style={{ marginRight: 5, fontSize: 18 }} />
          WAHYU FATUR RIZKI
        </NavLink>
      </NavItem>
        <Container>
          <Form
            className="w-100"
            onSubmit={this.props.handleSubmit(this.onSearchPokemon)}
          >
            <FormGroup>
              <Field
                name="search"
                component={this.renderField}
                props={this.props}
              />
            </FormGroup>
          </Form>
        </Container>
      </Nav>
    );
  }
}

const validate: Validator = value => {
  const { search } = value;

  if (
    search &&
    pokemonNames.findIndex(
      availableName => availableName.toLowerCase() === search.toLowerCase()
    ) === -1
  )
    return { search: "Invalid Pokémon Name" };
};

const mapStateToProps = (state: IState) => {
  const { pokemons, currentPokemon, currentForm } = state.pokemon;
  const { name, palettes } = pokemons[currentPokemon][currentForm];

  return { name, palettes };
};

export default connect(mapStateToProps, {
  changePokemon
})(
  reduxForm<{}, INavbarProps>({
    form: "searchPokemon",
    validate
  })(Searchbar)
);
