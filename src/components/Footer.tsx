import React from 'react';
import { Navbar, NavItem, NavLink, Nav, Container } from 'reactstrap';
import { css } from 'emotion';
import { FaRegCopyright, FaLinkedin, FaGitlab } from 'react-icons/fa';

const { LinkedIn, gitlab } = {
  LinkedIn: "https://www.linkedin.com/in/wahyu-fatur-rizky/",
  gitlab: "https://gitlab.com/wahyufaturrizky"
};

const Footer = () => {
  const renderCopyright = () => (
    <Nav className="w-100 d-flex justify-content-center justify-content-lg-between">
      <NavItem>
        <NavLink className="text-center">
          <FaRegCopyright
            style={{ marginBottom: 3, marginRight: 5, fontSize: 15 }}
          />
          Nintendo, Game Freak, and The Pokémon Company
        </NavLink>
      </NavItem>
    </Nav>
  );

  const renderSocialMedia = () => (
    <Nav className="w-100 d-flex justify-content-center justify-content-lg-between">
      <NavItem>
        <NavLink className="text-white" href={LinkedIn} target="_blank">
          <FaLinkedin style={{ marginRight: 5, fontSize: 18 }} />
          WAHYU FATUR RIZKI
        </NavLink>
      </NavItem>
      <NavItem>
        <NavLink className="text-white" href={gitlab} target="_blank">
          <FaGitlab style={{ marginRight: 5, fontSize: 18 }} />
          WAHYU FATUR RIZKI
        </NavLink>
      </NavItem>
    </Nav>
  );

  return (
    <footer className="section-navbars">
      <div id="footer">
        <div className="navigation">
          <Navbar className={navigationStyle} expand="sm">
            <Container>
              {renderCopyright()}
              {renderSocialMedia()}
            </Container>
          </Navbar>
        </div>
      </div>
    </footer>
  );
};

const navigationStyle = css`
  z-index: 10;
  background: transparent;

  @media (max-width: 1024px) {
    padding-top: 10%;
  }

  @media (max-width: 480px) {
    padding-top: 5%;
  }
`;

export default Footer;
